<div align='center'>

# Script Após formatação do Linux
 Script utilizado por [Gigante TGames](https://gitlab.com/GiganteTGames) para instalações de frameworks e programas essenciais.

#### Atalho para ir para o comando de instalação específico

| [Git](#install-git) | [Snapd](#install-snapd) | [NodeJS](#install-nodejs) | [Flutter](#install-flutter) | [Android Studio](#install-android-studio) | [Visual Studio Code](#install-visual-studio-code) | [Chromium](#install-chromium) |
|:---------: | :------: | :------:| :------:| :------:| :------:| :------:|


## Install Git
---

```
sudo apt install git -y

sudo add-apt-repository ppa:git-core/ppa; sudo apt update ; sudo apt install git -y

```



## Install Snapd 
---

```
sudo rm /etc/apt/preferences.d/nosnap.pref
sudo apt update
sudo apt install snapd -y

```

## Install NodeJS
---

```
sudo apt install wget
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
source ~/.profile
nvm ls-remote
nvm install
```
### Se alterar o node e desinstalar a antiga versao para instalar uma nova usar o comando para n'ao ficar dando erro

```
nvm alias default node
nvm ls

```

## Install Flutter
---

```
snap install flutter --classic   

```
## Install Android Studio
---

```
snap install android-studio --classic

```
## Install Visual Studio Code
---

```
snap install code --classic

```
## Install Chromium
---

```
 sudo snap install chromium

```


</div>
